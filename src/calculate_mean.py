import cv2 as cv
import numpy as np


def calculate_mean_parameters(num_of_samples):
    # Lists to store given paremeters.
    cameramatrix = []
    dist = []

    # Getting intrinsic parameters from XML file obtained with camera_calibration.py
    calibration_file = cv.FileStorage("data/intrinsics.xml", cv.FILE_STORAGE_READ)

    for i in range(0,num_of_samples-1):
        cameramatrix.append(calibration_file.getNode("Camera_matrix_{}".format(i)).mat())

    calibration_file.release()

    # Getting distortion coefficients from XML file obtained with camera_calibration.py
    calibration_file = cv.FileStorage("data/distortion.xml", cv.FILE_STORAGE_READ)

    for i in range(0,num_of_samples):
        dist.append(calibration_file.getNode("Distortion_coefficients_{}".format(i)).mat())

    calibration_file.release()

    mean_mtx = np.mean(cameramatrix, axis=0)
    mean_dist = np.mean(dist, axis=0)

    mtx_std = np.std(cameramatrix, axis=0)
    dist_std = np.std(dist, axis=0)

    return (mean_mtx, mean_dist, mtx_std, dist_std)
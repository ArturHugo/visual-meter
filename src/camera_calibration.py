import numpy as np
import cv2 as cv

from calculate_mean import calculate_mean_parameters
from image_controller import ImagePathController, selection_callback

# termination criteria
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((6*8,3), np.float32)
objp[:,:2] = np.mgrid[0:8,0:6].T.reshape(-1,2)
objp *= 3   # Since the distance between two points is approximately 3 cm, this gives
            # us the the points coordinates in real world.

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.


# Method to get point on world coordinate frame.
def image_point_to_wcf(im_point, rvec, tvec, intrinsics):
    rmat = cv.Rodrigues(rvec)
    # Removing 3rd column of rotation matrix assuming wcf point is aligned with xy plane.
    rmat = np.delete(rmat[0], 2, 1)
    extrinsics = np.concatenate((rmat, tvec), axis=1)
    # Putting image point in homogeneous coordinates.
    im_point = np.concatenate((im_point, 1), axis=None)
    wcf_point = np.matmul(np.matmul(np.linalg.inv(extrinsics),np.linalg.inv(intrinsics)),im_point)
    wcf_point = wcf_point/wcf_point[2]
    wcf_point = np.delete(wcf_point, 2, 0)
    return wcf_point

# Defining constants related to line drawing.
RADIUS = 3
RED = (0, 0, 255)

# Defining window names.
RAW = 'raw'
UND = 'undistorted'
CHESS = 'chessboard'

# Capturing video, opening windows and setting callbacks.
capture = cv.VideoCapture(0)

ret, img = capture.read()

cv.namedWindow(RAW, cv.WINDOW_AUTOSIZE)
cv.namedWindow(UND, cv.WINDOW_AUTOSIZE)

path_raw = ImagePathController(img, RAW)
path_und = ImagePathController(img, UND)

cv.setMouseCallback(RAW, selection_callback, path_raw)
cv.setMouseCallback(UND, selection_callback, path_und)

# Flag to see if a callibration was made.
got_params = False

# Cleaning files.
open('data/intrinsics.xml', 'w').close()
open('data/distortion.xml', 'w').close()

# Index of calibration matrices to save on xml file.
id = 0

# To control console output.
printed_und_measure = False
printed_raw_measure = False

while 1:
    ret, img = capture.read()

    path_raw.image = img
    path_und.image = img

    gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)

    key = cv.waitKey(1) & 0xFF

    # Press c to capture frame and find chessboard.
    if key == 99:
        # Find the chess board corners
        ret, corners = cv.findChessboardCorners(gray, (8,6),None)

        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)

            corners2 = cv.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
            imgpoints.append(corners2)

            # Draw and display the corners
            # chessboard_img = cv.drawChessboardCorners(img, (8,6), corners2,ret)
            # cv.imshow(CHESS, chessboard_img)

            if id < 5:
                ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
                print("Norm of tvec calculated simultaneously with intrinsics: {}cm".format(cv.norm(tvecs[id])))

            # From 5 samples and onwards, calibrate extrinsics with average values of
            # the parameters previously calculated.
            if id >= 5:
                mtx, dist, mtx_std, dist_std = calculate_mean_parameters(id)

                ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],mtx,dist)
                
                print("Norm of tvec post intrinsics: {}cm".format(cv.norm(tvecs[id])))


            h, w = img.shape[:2]
            newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))

            # Opening xml files to store parameter samples.
            intrinsics_file = cv.FileStorage("data/intrinsics.xml", cv.FILE_STORAGE_APPEND)
            dist_file = cv.FileStorage("data/distortion.xml", cv.FILE_STORAGE_APPEND)

            # Writing XML files with obtained parameters.
            intrinsics_file.write("Camera_matrix_{}".format(id), newcameramtx)
            dist_file.write("Distortion_coefficients_{}".format(id), dist)
            intrinsics_file.release()
            dist_file.release()

            id += 1
            got_params = True

    elif key == 114:
        printed_und_measure = False
        printed_raw_measure = False

    elif key == 27:
        break

    else:    
        if got_params:
            # undistort
            dst = cv.undistort(img, mtx, dist, None, newcameramtx)

            if path_und.initial_point != None:
                xi, yi = path_und.initial_point.x, path_und.initial_point.y
                center_initial = (xi, yi)
                cv.circle(dst, center_initial, RADIUS, RED)
                if path_und.final_point != None:
                    xf, yf = path_und.final_point.x, path_und.final_point.y
                    center_final = (xf, yf)
                    cv.circle(dst, center_final, RADIUS, RED)
                    cv.line(dst, center_initial, center_final, RED)
                    wcf_initial = image_point_to_wcf([xi,yi], rvecs[id-2], tvecs[id-2], newcameramtx)
                    wcf_final = image_point_to_wcf([xf,yf], rvecs[id-2], tvecs[id-2], newcameramtx)
                    if not printed_und_measure:
                        real_length = cv.norm(wcf_final - wcf_initial)
                        print("Lengths in window {}: {}px {}cm".format(path_und.window, path_und.length, real_length))
                        printed_und_measure = True

            cv.imshow(UND,dst)

        if path_raw.initial_point != None:
            xi, yi = path_raw.initial_point.x, path_raw.initial_point.y
            center_initial = (xi, yi)
            cv.circle(img, center_initial, RADIUS, RED)
            if path_raw.final_point != None:
                printed_measure = True
                xf, yf = path_raw.final_point.x, path_raw.final_point.y
                center_final = (xf, yf)
                cv.circle(img, center_final, RADIUS, RED)
                cv.line(img, center_initial, center_final, RED)
                wcf_initial = image_point_to_wcf([xi,yi], rvecs[id-2], tvecs[id-2], newcameramtx)
                wcf_final = image_point_to_wcf([xf,yf], rvecs[id-2], tvecs[id-2], newcameramtx)
                if not printed_raw_measure:
                    real_length = cv.norm(wcf_final - wcf_initial)
                    print("Lengths in window {}: {}px {}cm".format(path_raw.window, path_raw.length, real_length))
                    printed_raw_measure = True

        cv.imshow(RAW,img)

cv.destroyAllWindows()

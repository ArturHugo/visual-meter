import numpy as np
import cv2 as cv

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

# Class to controll state of image displayed in opencv window
class ImagePathController:
    def __init__(self, image, window):
        self.image = image
        self.window = window
        self.initial_point = None        # Origin of the path to be traced in the image.
        self.final_point = None          # Destiny of the path to be traced.
        self.length = 0                  # Length in pixels.

    def select_initial(self, x, y):
        self.initial_point = Point(x, y)
    
    def select_final(self, x, y):
        self.final_point = Point(x, y)


# Mouse callback to select the points that make the path.
def selection_callback(event, x, y, flags, path):
    if event == cv.EVENT_LBUTTONDOWN:
        if path.initial_point == None:
            path.select_initial(x, y)

        elif path.final_point == None:
            path.select_final(x, y)
            # Calculating path length.
            initial = np.array([path.initial_point.x, path.initial_point.y])
            final = np.array([path.final_point.x, path.final_point.y])
            path.length = cv.norm(initial - final)

        else:
            path.final_point = None
            path.select_initial(x, y)
            path.length = 0

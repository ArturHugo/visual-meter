# Visual meter
This project has the goal of exercise the aspects of camera calibration by developing a simplified visual meter. It was   developed with OpenCV 3.4.4, Numpy 1.11.1 and Python 3.5.1

## Structure
The main module is calibrate_camera.py, which contains the main loop of rendering frames from the webcam video.

The auxiliary modules are: 
- calculate_mean.py, which reads matrices of parameters from xml files in the data folder and takes the mean and the standard deviation of them.
- image_controller.py, which contains the class to control the lines drawn on the scream by the user.

## Usage
### Camera calibration
By running src/calibrate_camera.py the program will open two windows: raw and undistorted. Raw will show the raw video of the webcam, while unidstorted will be empty at first.

To calibrate the camera, place a 9x7 chessboard pattern in front of the camera and press 'c'. This will trigger the algorithm that will search the pattern points in the captured frame.

Once the algorithm runs in the snapshot, it will calibrate the intrinsic parameters of the camera (camera matrix coefficients as well as the distortion coefficients) and store those values in the files data/intrincs.xml and data/distortion.xml

For each calibration you get one sample of the intrinsic parameters. For the first five, the program calibrates the extrinsic parameters simultaneously and displays the estimated distance from camera to pattern in the terminal in centimeters. For the next calibrations the application will call calculate_mean_parameters to get a better estimation of the intrinsics and feed it to the calibrateCamera method from OpenCV, so it gets a more accurate estimation of the extrinsics as well.

For every extrinsic calibrated the program will display the norm of the translation vector in the terminal, which approximates the distance from the camera to the pattern.

### Measuring objects
In both windows of video, you can left click with the mouse to select a pixel. By selecting two pixels on the same window a line will be drawn connecting them. The first time you set a line, it's length will be displayed in the console, both in pixels and centimeters. After that, you will need to press 'r' when you set a new line in order to display it's length.

Once you are done testing the program, press 'esc' to close the windows and finish the script.

>Link to the remote repository: https://gitlab.com/ArturHugo/visual-meter